<?php
/*
Template Name: CE Quiz Template
*/

add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' ); // Force Full-Width Layout
get_header();
?>

<style>
#sidebar_wrapper {
    float: left;
    height: 100%;
    margin-left: auto;
    margin-right: 25px;
    width: 150px;
</style>

<!-- Get content without the sidebar -->

<div id='content'>
	<?php
	include_once ($_SERVER['DOCUMENT_ROOT'].'/php/forms/header.php');
	include_once ($_SERVER['DOCUMENT_ROOT'].'/php/forms/sidebar.php');
?>
<div id="forms-container" style="float:right; width:900px;">
	<?php
	switch($_GET['type'])
	{
		case 'postcon':
			include ($_SERVER['DOCUMENT_ROOT'].'/php/forms/postcon.php');
			break;
		case 'precon':
			include ($_SERVER['DOCUMENT_ROOT'].'/php/forms/precon.php');
			break;
		case 'submitted':
			include ($_SERVER['DOCUMENT_ROOT'].'/php/forms/submitted.php');
			break;
		case 'submitted-selection':
			include ($_SERVER['DOCUMENT_ROOT'].'/php/forms/submitted-selection.php');
			break;
		case 'submitted-postcon':
			include ($_SERVER['DOCUMENT_ROOT'].'/php/forms/submitted-postcon.php');
			break;
		case 'evaluation':
			include ($_SERVER['DOCUMENT_ROOT'].'/php/forms/evaluation.php');
			break;
		case 'evals':
			include ($_SERVER['DOCUMENT_ROOT'].'/php/forms/evals.php');
			break;
		case 'evalsubmit':
			include ($_SERVER['DOCUMENT_ROOT'].'/php/forms/evalsubmit.php');
			break;
		case 'cecme':
			include ($_SERVER['DOCUMENT_ROOT'].'/php/forms/cecme.php');
			break;
		case 'session-selection':
			include ($_SERVER['DOCUMENT_ROOT'].'/php/forms/session-selection.php');
			break;
		case 'result':
			include ($_SERVER['DOCUMENT_ROOT'].'/php/forms/result.php');
			break;
case 'resulttest':
			include ($_SERVER['DOCUMENT_ROOT'].'/php/forms/result_test.php');
			break;
		default:
			include ($_SERVER['DOCUMENT_ROOT'].'/php/forms/postcon.php');
		}
	?>
</div></div>

<?php get_footer(); ?>

